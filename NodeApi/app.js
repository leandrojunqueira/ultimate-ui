const express = require("express");
const app = express();
const data = require('./intents.json')

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  next();
});
app.listen(3001, () => {
 console.log("Server running on port 3001");
});
app.get('/intents', (req, res) => {
    res.send(data);
  });
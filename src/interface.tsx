export interface Intents {
    index: number;
    id: string
    name: string
    description: string
    trainingData: TrainingData
    reply: Reply
    prevState: null
  }
  
  export interface TrainingData extends Intents {
    expressionCount: number
    expressions: Expression[]
  }
  
  export interface Expression extends TrainingData{
    id: string
    text: string
  }
  
  export interface Reply extends Intents{
    id: string
    text: string
  }
  
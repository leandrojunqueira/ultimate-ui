
import React from 'react';

// interface to declare types
interface Props {
  children: React.ReactNode;
  onClick: () => void; 
  custom?: string;
  disabled?: boolean;
}

const Button: React.FC<Props> = ({
  children,
  onClick,
  custom,
  disabled,
  ...rest
}) => {
  return (
    <button
      disabled={disabled}
      className={`btn` + ' ' + custom  }
      onClick={onClick}
      {...rest}
    >
      {children}
    </button>
  );
};

export default Button;

import React, {useState} from "react";
import Button from "./buttonSelect";
import next from '../assets/next.svg';
import back from '../assets/back.svg';

const Steps = ({ children, fields, disabled }) => {
  const [activePageIndex, setActivePageIndex] = useState(0);
  const pages = React.Children.toArray(children);
  const currentPage = pages[activePageIndex];

  const goNextPage = () => {
    setActivePageIndex(index => index + 1);
  };

  const goPrevPage = () => {
    setActivePageIndex(index => index - 1);
  };

  const ButtonPrev = () =>
    activePageIndex > 0 ? (
      <Button
        custom=" step-button"
        onClick={goPrevPage}
      ><img src={back} alt="back"/> Back </Button>
    ) : null;
  const ButtonNext = () =>
    activePageIndex < pages.length - 1 ? (
      <Button
        disabled={disabled}
        custom="step-button"
        onClick={goNextPage}
    >Next <img src={next} alt="next"/></Button>
    ) : null;

  return (
    <div className="wizard">
      <div className="steps">{currentPage}</div>
      <div className="steps-btn">
        <ButtonPrev />
        <ButtonNext />
      </div>
    </div>
  );
};

export default Steps;
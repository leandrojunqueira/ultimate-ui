import { Intents } from "./interface"

export const url: string = "http://localhost:3001/intents";

export async function fetchApi(url: RequestInfo) {
    try {
      const res = await fetch(url);
      const json = await res.json();
      console.log("check if data is loading: ", json);
      return json;
    } catch (e) {
      throw e;
    }
}
    

export async function restIntends(): Promise<Array<Intents>> {
    let resJson: any;
    try {
       let response = await fetch(url);
       resJson = response.json(); //JSON:

        await new Promise((resolve, reject) => {
                  if(resolve) {    
                      setTimeout(resolve, 1000); 
                    //   console.log('1 seconds  to load', resJson);
                  } else {    
                      reject('Promise is rejected');  
                  }
              });
    } catch(error) {
      console.log(error);
      throw error;
    } finally {
        console.log('All good');
    }
    
    return resJson;
  }
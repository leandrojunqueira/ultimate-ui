import React, { useEffect, useState } from 'react';
import './App.css';
import { fetchApi, url } from './serviceRest';
import { Expression, Intents, TrainingData } from './interface';
import Button from './components/buttonSelect';
import AddIcon from './assets/add.svg';
import RemoveIcon from './assets/remove.svg';
import Steps from './components/steps';

interface Props {
  data: any
  fields: any[]
}

function App() {
  const [data, setData] = React.useState<Intents[]>();
  const [add, setAdd] = React.useState(false);
  const [selectField, setSelectField] = React.useState<string[]>([]);


  // Add all fields
  const AddAll = () => { 
    return data!.map((m:Intents) =>  {
      setSelectField(prevState => [...prevState, m.id]);
      setAdd(true);
    });
  }
  // Remove all field
  const RemoveAll = () => { 
      setAdd(false);
      return setSelectField([]);
  }
  const CheckIfExit = (value: any) => {
      return selectField.some((s) =>  s === value);
  } 
  // Add single field 
  const Add = (id: string) => {
        if(CheckIfExit(id)) {
          return alert('Alread added successfully')
        }
          setSelectField(prevState => [...prevState, id]);
  }
  // Remove single selection
  const Remove = (id: string) => {
      const removing = selectField.filter(i => i !== id)
      return setSelectField(removing);
  }

//Check array of ids is finder in data
const FilterIntents = (ids:any) => data!.filter(f => ids.includes(f.id));

//Check if no fields is selected
const disabledButton  = () => selectField.length <= 0 ? true : false; 


const Step1: React.FC<Props> = ({
  data,
  fields
}) => {
        return (
        <div>  
         <Button 
            onClick={() => add ? RemoveAll() : AddAll()}
            custom="addRemoveAll"
            > 
            {add ?  <span> Remove All <img src={RemoveIcon} /></span> : <span> Select All <img src={AddIcon} /> </span> }
          </Button>
            { data && data!.map((m:Intents) => {
            return  ( 
              <Button 
                onClick={() => CheckIfExit(m.id) ? Remove(m.id) : Add(m.id)}
                custom='active'
              > 
               {m.name} - {m.description} {CheckIfExit(m.id) ? <img src={RemoveIcon} alt="remove"/>: <img src={AddIcon} alt="Add"/>}
            </Button>
            )
            })}
        </div>
        )
    }

    const Step2: React.FC<Props> = ({
      data,
      fields
    }) => {
  
      return (
      <div>  
          { selectField && FilterIntents(fields).map((m:Intents, index) => {
          return  ( 
            <div className="interations" key={index}>
              <p><span className="topic">{m.name}:</span> {data && m.trainingData.expressions.map((m:Expression) => <span>{m.text}, </span>)}</p>
              <p> <span className="reply">Reply: </span> {m.reply.text}</p>
            </div>
          )
          })}
      </div>
      )
  }
  const Step3: React.FC<Props> = ({
    data,
    fields
  }) => {
    return (
      <div><h1>Test your interation</h1>
      <p>I'd create a live chat for testing all interaction. </p>
        {/* <p>{JSON.stringify(fields)}</p> */}
        </div>

    )
  }
  useEffect (() => {
    fetchApi(url).then((d) => {
      setData(d);
    })

  }, []);

  return (
    <div className="App">
      <div className="container">
        <h1>Select chat interations.</h1>
        <div className="box">
        </div>
        <div className="content>">
          <Steps fields={selectField} disabled={disabledButton()} >
            <Step1 data={data} fields={selectField} ></Step1>
            <Step2  data={data} fields={selectField} ></Step2>
            <Step3 data={data} fields={selectField}></Step3>
          </Steps>
        
        </div>
      </div>
  </div>
  );
}

export default App;


